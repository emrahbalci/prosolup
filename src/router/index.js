import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/views/HomePage'
import Benefits from '@/views/Benefits'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/benefits',
      name: 'Benefits',
      component: Benefits
    }
  ]
})
